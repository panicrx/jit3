package pg

import (
	"gitlab.com/panicrx/jit3/contrib/pg/pgsyntax"
	"gitlab.com/panicrx/jit3/contrib/pg/pgtoken"
	"gitlab.com/panicrx/jit3/pkg/dialect"
	"gitlab.com/panicrx/jit3/pkg/scope"
	"gitlab.com/panicrx/jit3/pkg/span"
	"gitlab.com/panicrx/jit3/pkg/symbol"
	"gitlab.com/panicrx/jit3/pkg/syntax"
	"gitlab.com/panicrx/jit3/pkg/token"
)

type (
	TokenType   = pgtoken.TokenType
	NodeType    = pgsyntax.NodeType
	ObjectType  struct{}
	DataType    struct{}
	Scope       = scope.Scope[ObjectType, *DataType]
	Pos         = span.Pos
	Span        = span.Span
	Spans       = span.Spans
	Range       = span.Range
	TextSpan    = span.TextSpan
	TextSpans   = span.TextSpans
	TokenSpan   = token.TokenSpan[TokenType]
	Token       = token.Token[TokenType]
	SyntaxNode  = syntax.Node[NodeType, TokenType]
	SymbolNode  = symbol.Node[ObjectType, *DataType, NodeType, TokenType]
	dialectType = dialect.Dialect[ObjectType, *DataType, NodeType, TokenType]
)

func (dt *DataType) String() string {
	panic("not implemented")
}

type Dialect struct{}

var _ dialectType = &Dialect{}

func (d *Dialect) Parse([]TokenSpan) (SyntaxNode, error) {
	// TODO implement pg.Parser
	panic("not implemented")
}

func (d *Dialect) Symbolize(Scope, SyntaxNode) (SymbolNode, error) {
	// TODO implement pg.Symbolizer
	panic("not implemented")
}

func (d *Dialect) EmitSyntax(SymbolNode) (SyntaxNode, error) {
	panic("not implemented")
}

func (d *Dialect) EmitTokens(SyntaxNode) ([]Token, error) {
	panic("not implemented")
}

func (d *Dialect) SpanTokens([]Token) (Spans, error) {
	panic("not implemented")
}

func (d *Dialect) Format(Spans) (TextSpans, error) {
	panic("not implemented")
}
