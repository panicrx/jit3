package pg_test

import (
	"testing"

	"gitlab.com/panicrx/jit3/contrib/pg"
	"gitlab.com/panicrx/jit3/pkg/testutil"
)

func AssertPosEquals(t *testing.T, expectedI, actualI any, _ ...any) bool {
	t.Helper()

	return testutil.AssertPosEquals(t, expectedI, actualI)
}

func AssertSpanEquals(t *testing.T, expectedI, actualI any, _ ...any) bool {
	t.Helper()

	return testutil.AssertSpanEquals(t, expectedI, actualI)
}

func AssertTextSpanEquals(t *testing.T, expectedI, actualI any, _ ...any) bool {
	t.Helper()

	return testutil.AssertTextSpanEquals(t, expectedI, actualI)
}

func AssertTokenSpanEquals(t *testing.T, expectedI, actualI any, _ ...any) bool {
	t.Helper()

	return testutil.AssertTokenSpanEquals[pg.TokenType](t, expectedI, actualI)
}

func AssertTokenSpansEquals(t *testing.T, expectedI, actualI any, _ ...any) bool {
	t.Helper()

	return testutil.AssertTokenSpansEquals[pg.TokenType](t, expectedI, actualI)
}

func AssertRangeEquals(t *testing.T, expected, actual pg.Range, _ ...any) bool {
	t.Helper()

	return testutil.AssertRangeEquals(t, expected, actual)
}
