package pg_test

import (
	_ "embed"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/panicrx/jit3/contrib/pg"
	"gitlab.com/panicrx/jit3/contrib/pg/pgtoken"
	"gitlab.com/panicrx/jit3/pkg/span"
	"gitlab.com/panicrx/jit3/pkg/text"
	"gitlab.com/panicrx/jit3/pkg/token"
)

func TestTokenize(t *testing.T) {
	t.Run("tokenize01.sql", func(t *testing.T) {
		spans := tokenizeTextsBuildTexts(t, "tokenize01.sql")
		sut := pg.Dialect{}
		actual, actualErr := sut.Tokenize(spans)
		assert.NoError(t, actualErr)

		expected := []pg.TokenSpan{
			token.NewSpan(
				pgtoken.Ampersand,
				span.NewTextSpan(span.NewSpanString(""), span.NewRange(span.NewPos(0, 1, 0), span.NewPos(0, 1, 0))),
			),
		}
		AssertTokenSpansEquals(t, expected, actual)
	})
}

func tokenizeTextsBuildTexts(t *testing.T, names ...string) *span.TextSpans {
	t.Helper()

	txts := new(text.Texts)
	for _, n := range names {
		txts.Add(n, tokenizeTestData[n])
	}

	dialect := pg.Dialect{}
	spans, err := dialect.SpanTexts(txts)
	if !assert.NoError(t, err) {
		return nil
	}

	return spans
}

var (
	//go:embed testdata/tokenize/tokenize01.sql
	tokenize01 string

	tokenizeTestData = map[string]string{
		"tokenize01.sql": tokenize01,
	}
)
