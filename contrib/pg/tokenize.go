package pg

import (
	"fmt"
	"regexp"
	"strings"

	"gitlab.com/panicrx/jit3/contrib/pg/pgtoken"
	"gitlab.com/panicrx/jit3/pkg/span"
	"gitlab.com/panicrx/jit3/pkg/token"
)

func (d *Dialect) Tokenize(spans *span.TextSpans) ([]TokenSpan, error) {
	buf := make([]span.TextSpan, 0, spans.Len())
	for ndx := 0; ndx < spans.Len(); ndx++ {
		buf = append(buf, spans.Span(ndx))
	}

	var ret []TokenSpan
	var cur span.TextSpan
	for len(buf) > 0 {
		cur, buf = tknConsume(buf)

		switch {
		case tknMatchesAny(cur, rxpGrpWhitespace):
			continue
		case tknMatchesAny(cur, rxpGrpComments):
			continue
		case tknMatchesAny(cur, rxpGrpIdents):
			ret = append(ret, token.NewSpan(pgtoken.Ident, cur))
			continue
		case tknMatchesAny(cur, rxpGrpNumbers):
			ret = append(ret, token.NewSpan(pgtoken.LiteralNum, cur))
			continue
		case tknMatchesAny(cur, rxpGrpStrings):
			ret = append(ret, token.NewSpan(pgtoken.LiteralStr, cur))
			continue
		case tknIsDollarString(cur):
			ret = append(ret, token.NewSpan(pgtoken.LiteralStr, cur))
			continue
		case tknMatchesAny(cur, rxpGrpSymbols):
			// TODO parse symbols correctly
			ret = append(ret, token.NewSpan(pgtoken.Ampersand, cur))
			continue
		default:
			return nil, errNoMatch
		}
	}

	return ret, nil
}

func tknMatches(cur span.TextSpan, rxp *regexp.Regexp) bool {
	return rxp.MatchString(cur.String())
}

func tknMatchesAny(cur span.TextSpan, rxpGrp []*regexp.Regexp) bool {
	for _, rxp := range rxpGrp {
		if rxp.MatchString(cur.String()) {
			return true
		}
	}
	return false
}

func tknMatchSymbol(cur span.TextSpan, buf []span.TextSpan) (span.TextSpan, []span.TextSpan, TokenSpan) {
	run, hasNonStandard := func(cur span.TextSpan, buf []span.TextSpan) (run []span.TextSpan, hasNonStandard bool) {
		for tknMatchesAny(cur, rxpGrpSymbols) {
			run = append(run, cur)
			hasNonStandard = hasNonStandard || tknMatches(cur, rxpSymbolsOperatorNonStandard)

			cur, buf = tknConsume(buf)
		}
		return
	}(cur, buf)

	retCur, retBuf, retSpan := func(buf []span.TextSpan, run []span.TextSpan, hasNonStandard bool) (span.TextSpan, []span.TextSpan, TokenSpan) {
		switch {
		case len(run) == 1:
			t := pgtoken.TokenType(run[0].String()[0])
			if !t.Defined() {
				panic(fmt.Sprintf("pg: symbol token not defined: %v", t))
			}
			return cur, buf, token.NewSpan(t, run...)
		case hasNonStandard:
			return run[len(run)-1], buf[len(run)-1:], token.NewSpan(pgtoken.CustomOperator, run...)
		default:
			for i := len(run) - 1; i > 0; i-- {
				switch run[i].String() {
				case "+", "-":
					continue
				default:
					if i >= len(run)-1 {
						return run[len(run)-1], buf[len(run)-1:], token.NewSpan(pgtoken.CustomOperator, run...)
					}
					return run[i], buf[i:], token.NewSpan(pgtoken.CustomOperator, run[0:i]...)
				}
			}

			t := pgtoken.TokenType(run[0].String()[0])
			if !t.Defined() {
				panic(fmt.Sprintf("pg: symbol token not defined: %v", t))
			}
			return run[0], buf[1:], token.NewSpan(t, run[:1]...)
		}
	}(buf, run, hasNonStandard)

	switch retSpan.Span().String() {
	case "<=":
		retSpan = token.WithTypeSpan(retSpan, pgtoken.LessEqual)
	case ">=":
		retSpan = token.WithTypeSpan(retSpan, pgtoken.MoreEqual)
	case "<>":
		retSpan = token.WithTypeSpan(retSpan, pgtoken.LessMore)
	case "!=":
		retSpan = token.WithTypeSpan(retSpan, pgtoken.BangEqual)
	case "|/":
		retSpan = token.WithTypeSpan(retSpan, pgtoken.PipeSlash)
	case "||/":
		retSpan = token.WithTypeSpan(retSpan, pgtoken.PipePipeSlash)
	case "!!":
		retSpan = token.WithTypeSpan(retSpan, pgtoken.BangBang)
	case "<<":
		retSpan = token.WithTypeSpan(retSpan, pgtoken.LessLess)
	case ">>":
		retSpan = token.WithTypeSpan(retSpan, pgtoken.MoreMore)
	case "||":
		retSpan = token.WithTypeSpan(retSpan, pgtoken.PipePipe)
	case "@@":
		retSpan = token.WithTypeSpan(retSpan, pgtoken.AtsignAtsign)
	}
	return retCur, retBuf, retSpan
}

func tknIsDollarString(cur span.TextSpan) bool {
	str := cur.String()
	dollarTag := rxpStringDollarPrefix.FindString(str)
	if len(dollarTag) == 0 {
		return false
	}

	prefix := str[len(dollarTag):]
	if next := strings.Index(prefix, dollarTag); next < 0 {
		return false
	}

	return true
}

func tknConsume(buf []span.TextSpan) (span.TextSpan, []span.TextSpan) {
	return buf[0], buf[1:]
}
