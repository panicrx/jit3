package pg

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"regexp"
	"unicode/utf8"

	"gitlab.com/panicrx/jit3/pkg/span"
	"gitlab.com/panicrx/jit3/pkg/text"
)

func (d *Dialect) SpanTexts(txts *text.Texts) (*span.TextSpans, error) {
	var spans []span.TextSpan

	var err error
	for n := 0; n < txts.Len(); n++ {
		txt := txts.Index(n)

		spans, err = spanText(spans, txt)
		if err != nil {
			return nil, err
		}
	}

	return span.NewTextSpans(txts, spans), nil
}

func spanText(spans []span.TextSpan, txt *text.Text) ([]span.TextSpan, error) {
	bs, err := io.ReadAll(txt.Reader())
	if err != nil {
		return nil, err
	}

	for len(bs) > 0 {
	fsloop:
		for _, f := range spanFuncs {
			spans, bs, err = f(spans, bs)
			switch {
			case errors.Is(err, errNoMatch):
				err = nil
				continue
			case err != nil:
				return nil, err
			default:
				break fsloop
			}
		}
	}

	if len(bs) > 0 {
		return nil, errNoMatch
	}

	return spans, nil
}

var errNoMatch = errors.New("no match")

var spanFuncs = []spanFunc{
	spanWhitespace,
	spanComments,
	spanNumericConstants,
	spanStringConstants,
	spanSymbolicChars,
	spanIdents,
}

type spanFunc func(spans []span.TextSpan, bs []byte) ([]span.TextSpan, []byte, error)

func spanWithRegexp(pats []*regexp.Regexp, spans []span.TextSpan, bs []byte) ([]span.TextSpan, []byte, error) {
	var err error

	for _, pat := range pats {
		found := pat.Find(bs)
		if len(found) == 0 {
			continue
		}

		spans, err = spanAdd(spans, bs[:len(found)])
		if err != nil {
			return nil, nil, err
		}

		return spans, bs[len(found):], nil
	}

	return spans, bs, errNoMatch
}

func spanAdd(spans []span.TextSpan, bs []byte) ([]span.TextSpan, error) {
	if len(bs) == 0 {
		panic("pg: cannot add empty span")
	}

	var start span.Pos
	if len(spans) > 0 {
		start = spans[len(spans)-1].Range().End()
	} else {
		start = span.NewPos(0, 1, 1)
	}

	s := span.NewSpan(bs)

	endPos := start.Pos().Plus(len(bs))
	endRow := start.Row()
	endCol := start.Col()

	for len(bs) > 0 {
		switch r, size := utf8.DecodeRune(bs); r {
		case utf8.RuneError:
			return nil, fmt.Errorf("pg: invalid rune found at %d:%d", endRow, endCol)
		case '\n':
			endRow++
			endCol = 0
			fallthrough
		default:
			endCol++
			bs = bs[size:]
		}
	}

	end := span.NewPos(endPos, endRow, endCol)
	spans = append(spans, span.NewTextSpan(s, span.NewRange(start, end)))
	return spans, nil
}

func spanWhitespace(spans []span.TextSpan, bs []byte) ([]span.TextSpan, []byte, error) {
	return spanWithRegexp(rxpGrpWhitespace, spans, bs)
}

func spanComments(spans []span.TextSpan, bs []byte) ([]span.TextSpan, []byte, error) {
	spans, bs, err := spanWithRegexp(rxpGrpComments, spans, bs)
	if errors.Is(err, errNoMatch) {
		unmatched := rxpCommentBlockOpen.Find(bs)
		if len(unmatched) > 0 {
			return nil, nil, errors.New("pg: closing block comment tag not found: /*")
		}
	}
	return spans, bs, err
}

func spanNumericConstants(spans []span.TextSpan, bs []byte) ([]span.TextSpan, []byte, error) {
	return spanWithRegexp(rxpGrpNumbers, spans, bs)
}

func spanStringConstants(spans []span.TextSpan, bs []byte) ([]span.TextSpan, []byte, error) {
	var err error
	spans, bs, err = spanWithRegexp(rxpGrpStrings, spans, bs)
	switch {
	case errors.Is(err, errNoMatch):
		err = nil
	case err != nil:
		return nil, nil, err
	default:
		return spans, bs, nil
	}

	dollarTag := rxpStringDollarPrefix.Find(bs)
	if len(dollarTag) == 0 {
		return spans, bs, errNoMatch
	}

	next := bytes.Index(bs[len(dollarTag):], dollarTag)
	if next < 0 {
		return nil, nil, fmt.Errorf("pg: closing dollar tag not found: %s", string(dollarTag))
	}

	l := len(dollarTag) + next + len(dollarTag)

	spans, err = spanAdd(spans, bs[:l])
	if err != nil {
		return nil, nil, err
	}

	return spans, bs[l:], nil
}

func spanIdents(spans []span.TextSpan, bs []byte) ([]span.TextSpan, []byte, error) {
	return spanWithRegexp(rxpGrpIdents, spans, bs)
}

func spanSymbolicChars(spans []span.TextSpan, bs []byte) ([]span.TextSpan, []byte, error) {
	return spanWithRegexp(rxpGrpSymbols, spans, bs)
}
