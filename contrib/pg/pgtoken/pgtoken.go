package pgtoken

//go:generate go-enumerator
type TokenType int

const (
	Plus       TokenType = '+'
	Minus      TokenType = '-'
	Star       TokenType = '*'
	Slash      TokenType = '/'
	Less       TokenType = '<'
	More       TokenType = '>'
	Equal      TokenType = '='
	Tilde      TokenType = '~'
	Bang       TokenType = '!'
	Atsign     TokenType = '@'
	Pound      TokenType = '#'
	Percent    TokenType = '%'
	Carat      TokenType = '^'
	Ampersand  TokenType = '&'
	Pipe       TokenType = '|'
	Backtick   TokenType = '`'
	Question   TokenType = '?'
	Dollar     TokenType = '$'
	Lparen     TokenType = '('
	Rparen     TokenType = ')'
	Lsqbracket TokenType = '['
	Rsqbracket TokenType = ']'
	Comma      TokenType = ','
	Semicolon  TokenType = ';'
	Colon      TokenType = ':'
	Dot        TokenType = '.'
)

const (
	// LessEqual: <=
	LessEqual TokenType = (1 << 8) + iota
	// MoreEqual: >=
	MoreEqual
	LessMore
	BangEqual
	PipeSlash
	PipePipeSlash
	BangBang
	LessLess
	MoreMore
	PipePipe
	AtsignMinusAtsign
	AtsignAtsign
	PoundPound
	LessMinusMore
	AtsignMore
	LessAtsign
	AmpersandAmpersand
	AmpersandLess
	AmpersandMore
	LessLessPipe
	PipeMoreMore
	AmpersandLessPipe
	PipeAmpersandMore
	LessCaret
	MoreCaret
	QuestionPound
	QuestionMinus
	QuestionPipe
	QuestionPipePipe
	TildeEqual
	LessLessEqual
	MoreMoreEqual
	MinusMore
	MinusMoreMore
	PoundMore
	PoundMoreMore
	QuestionAmpersand
	PoundMinus
	AtsignQuestion
	MinusPipeMinus

	CustomOperator
)

const (
	Ident TokenType = (1 << 9) + iota
	LiteralStr
	LiteralNum
	Null
	Unknown
	True
	False
)

const (
	keywordStart TokenType = (1 << 10) + iota
	Select
	As
	Case
	When
	Then
	End
	From
	Where
	By
	Group
	Having
	Order
	Asc
	Desc
	In
	Is
	Like
	ILike
	Similar
	Insert
	Into
	Values
	Update
	Set
	Symmetric
	And
	Or
	Not
	Between
	IsNull
	NotNull
	Distinct
	Coalesce
	NullIf
	Greatest
	Least
	keywordStop
)

func (t TokenType) IsKeyword() bool {
	return t > keywordStart && t < keywordStop
}
