package pg_test

import (
	"testing"

	_ "embed"

	"github.com/stretchr/testify/assert"
	"gitlab.com/panicrx/jit3/contrib/pg"
	"gitlab.com/panicrx/jit3/pkg/span"
	"gitlab.com/panicrx/jit3/pkg/text"
)

func TestSpanTexts(t *testing.T) {
	t.Run("spanTexts01.sql", func(t *testing.T) {
		txts := spanTextsBuildTexts(t, "spanTexts01.sql")
		sut := pg.Dialect{}

		switch got, err := sut.SpanTexts(txts); {
		case !assert.NoError(t, err):
		case !assert.Same(t, txts, got.Texts):
		case !assert.Equal(t, 11, got.Len()):
		default:
			tcs := []struct {
				n  int
				tc span.TextSpan
			}{
				{
					n:  0,
					tc: span.NewTextSpan(span.NewSpanString("select"), span.NewRange(span.NewPos(text.Pos(0), 1, 1), span.NewPos(text.Pos(6), 1, 7))),
				},
				{
					n:  1,
					tc: span.NewTextSpan(span.NewSpanString(" "), span.NewRange(span.NewPos(text.Pos(6), 1, 7), span.NewPos(text.Pos(7), 1, 8))),
				},
				{
					n:  2,
					tc: span.NewTextSpan(span.NewSpanString("*"), span.NewRange(span.NewPos(text.Pos(7), 1, 8), span.NewPos(text.Pos(8), 1, 9))),
				},
				{
					n:  3,
					tc: span.NewTextSpan(span.NewSpanString(" "), span.NewRange(span.NewPos(text.Pos(8), 1, 9), span.NewPos(text.Pos(9), 1, 10))),
				},
				{
					n:  4,
					tc: span.NewTextSpan(span.NewSpanString("from"), span.NewRange(span.NewPos(text.Pos(9), 1, 10), span.NewPos(text.Pos(13), 1, 14))),
				},
				{
					n:  5,
					tc: span.NewTextSpan(span.NewSpanString(" "), span.NewRange(span.NewPos(text.Pos(13), 1, 14), span.NewPos(text.Pos(14), 1, 15))),
				},
				{
					n:  6,
					tc: span.NewTextSpan(span.NewSpanString("public"), span.NewRange(span.NewPos(text.Pos(14), 1, 15), span.NewPos(text.Pos(20), 1, 21))),
				},
				{
					n:  7,
					tc: span.NewTextSpan(span.NewSpanString("."), span.NewRange(span.NewPos(text.Pos(20), 1, 21), span.NewPos(text.Pos(21), 1, 22))),
				},
				{
					n:  8,
					tc: span.NewTextSpan(span.NewSpanString("table"), span.NewRange(span.NewPos(text.Pos(21), 1, 22), span.NewPos(text.Pos(26), 1, 27))),
				},
				{
					n:  9,
					tc: span.NewTextSpan(span.NewSpanString(";"), span.NewRange(span.NewPos(text.Pos(26), 1, 27), span.NewPos(text.Pos(27), 1, 28))),
				},
				{
					n:  10,
					tc: span.NewTextSpan(span.NewSpanString("\n"), span.NewRange(span.NewPos(text.Pos(27), 1, 28), span.NewPos(text.Pos(28), 2, 1))),
				},
			}

			for _, tc := range tcs {
				if !AssertTextSpanEquals(t, tc.tc, got.Span(tc.n)) {
					break
				}
			}
		}
	})

	t.Run("spanTexts01.sql", func(t *testing.T) {
		txts := spanTextsBuildTexts(t, "spanTexts02.sql")
		sut := pg.Dialect{}

		switch got, err := sut.SpanTexts(txts); {
		case !assert.NoError(t, err):
		case !assert.Same(t, txts, got.Texts):
		case !assert.Equal(t, 34, got.Len()):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(0), 1, 1), span.NewPos(text.Pos(2), 1, 3), "42", got.Span(0)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(3), 2, 1), span.NewPos(text.Pos(6), 2, 4), "3.5", got.Span(2)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(7), 3, 1), span.NewPos(text.Pos(9), 3, 3), "4.", got.Span(4)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(10), 4, 1), span.NewPos(text.Pos(14), 4, 5), ".001", got.Span(6)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(15), 5, 1), span.NewPos(text.Pos(18), 5, 4), "5e2", got.Span(8)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(19), 6, 1), span.NewPos(text.Pos(27), 6, 9), "1.925e-3", got.Span(10)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(28), 7, 1), span.NewPos(text.Pos(36), 7, 9), "0b100101", got.Span(12)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(37), 8, 1), span.NewPos(text.Pos(47), 8, 11), "0B10011001", got.Span(14)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(48), 9, 1), span.NewPos(text.Pos(53), 9, 6), "0o273", got.Span(16)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(54), 10, 1), span.NewPos(text.Pos(59), 10, 6), "0O755", got.Span(18)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(60), 11, 1), span.NewPos(text.Pos(65), 11, 6), "0x42f", got.Span(20)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(66), 12, 1), span.NewPos(text.Pos(72), 12, 7), "0XFFFF", got.Span(22)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(73), 13, 1), span.NewPos(text.Pos(86), 13, 14), "1_500_000_000", got.Span(24)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(87), 14, 1), span.NewPos(text.Pos(106), 14, 20), "0b10001000_00000000", got.Span(26)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(107), 15, 1), span.NewPos(text.Pos(115), 15, 9), "0o_1_755", got.Span(28)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(116), 16, 1), span.NewPoQs(text.Pos(127), 16, 12), "0xFFFF_FFFF", got.Span(30)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(128), 17, 1), span.NewPos(text.Pos(137), 17, 10), "1.618_034", got.Span(32)):
		}
	})

	t.Run("spanTexts03.sql", func(t *testing.T) {
		txts := spanTextsBuildTexts(t, "spanTexts03.sql")
		sut := pg.Dialect{}

		switch got, err := sut.SpanTexts(txts); {
		case !assert.NoError(t, err):
		case !assert.Same(t, txts, got.Texts):
		case !assert.Equal(t, 8, got.Len()):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(0), 1, 1), span.NewPos(text.Pos(6), 1, 7), "'test'", got.Span(0)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(7), 2, 1), span.NewPos(text.Pos(14), 2, 8), "E'test'", got.Span(2)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(15), 3, 1), span.NewPos(text.Pos(23), 3, 9), "$$test$$", got.Span(4)):
		case !AssertTextSpanEquals(t, span.NewPos(text.Pos(24), 4, 1), span.NewPos(text.Pos(44), 4, 21), "$dollar$test$dollar$", got.Span(6)):
		}
	})

	t.Run("spanTexts04.sql", func(t *testing.T) {
		txts := spanTextsBuildTexts(t, "spanTexts04.sql")
		sut := pg.Dialect{}

		got, err := sut.SpanTexts(txts)

		switch {
		case !assert.NoError(t, err):
		case !assert.Same(t, txts, got.Texts):
		case !assert.Equal(t, 52, got.Len()):
		}

		s := 0
		for _, op := range []string{"+", "-", "*", "/", "<", ">", "=", "~", "!", "@", "#", "%", "^", "&", "|", "`", "?"} {
			AssertTextSpanEquals(t, span.NewPos(text.Pos(s), 1, s+1), span.NewPos(text.Pos(s+1), 1, s+2), op, got.Span(s))
			s += 2
		}

		for n, op := range []string{"+", "-", "*", "/", "<", ">", "=", "~", "!", "@", "#", "%", "^", "&", "|", "`", "?"}[:1] {
			AssertTextSpanEquals(t, span.NewPos(text.Pos(s), 2, n+1), span.NewPos(text.Pos(s+1), 2, n+2), op, got.Span(s))
			s += 1
		}
	})

	t.Run("spanTexts05.sql", func(t *testing.T) {
		txts := spanTextsBuildTexts(t, "spanTexts05.sql")
		sut := pg.Dialect{}

		got, err := sut.SpanTexts(txts)

		switch {
		case !assert.NoError(t, err):
		case !assert.Same(t, txts, got.Texts):
		case !assert.Equal(t, 6, got.Len()):
		default:
			ets := []struct {
				n  int
				ts pg.TextSpan
			}{
				{
					n: 0,
					ts: span.NewTextSpan(
						span.NewSpanString("var"),
						span.NewRange(
							span.NewPos(text.Pos(0), 1, 1),
							span.NewPos(text.Pos(3), 1, 4))),
				},
				{
					n: 2,
					ts: span.NewTextSpan(
						span.NewSpanString("var_$"),
						span.NewRange(
							span.NewPos(text.Pos(4), 2, 1),
							span.NewPos(text.Pos(9), 2, 6))),
				},
				{
					n: 4,
					ts: span.NewTextSpan(
						span.NewSpanString("v0ar_$"),
						span.NewRange(
							span.NewPos(text.Pos(10), 3, 1),
							span.NewPos(text.Pos(16), 3, 7))),
				},
			}

			for _, tc := range ets {
				if !AssertTextSpanEquals(t, tc.ts, got.Span(tc.n)) {
					break
				}
			}
		}
	})
}

func spanTextsBuildTexts(t *testing.T, names ...string) *text.Texts {
	t.Helper()

	ret := new(text.Texts)
	for _, n := range names {
		ret.Add(n, spanTextsTestData[n])
	}
	return ret
}

var (
	//go:embed testdata/spanTexts/spanTexts01.sql
	spanTexts01 string

	//go:embed testdata/spanTexts/spanTexts02.sql
	spanTexts02 string

	//go:embed testdata/spanTexts/spanTexts03.sql
	spanTexts03 string

	//go:embed testdata/spanTexts/spanTexts04.sql
	spanTexts04 string
	//
	//go:embed testdata/spanTexts/spanTexts05.sql
	spanTexts05 string

	spanTextsTestData = map[string]string{
		"spanTexts01.sql": spanTexts01,
		"spanTexts02.sql": spanTexts02,
		"spanTexts03.sql": spanTexts03,
		"spanTexts04.sql": spanTexts04,
		"spanTexts05.sql": spanTexts05,
	}
)
