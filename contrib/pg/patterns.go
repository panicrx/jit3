package pg

import (
	"fmt"
	"regexp"
)

const (
	rxpSubDec = `(\d+(\_\d+)*)`
	rxpSubHex = `([0-9A-Za-z]+(\_[0-9A-Za-z]+)*)`
	rxpSubOct = `([0-7]+(\_[0-7]+)*)`
	rxpSubBin = `([01]+(\_[01]+)*)`
)

var (
	rxpWhitespace                 = regexp.MustCompile(`(?ms)^\s+`)
	rxpCommentLine                = regexp.MustCompile(`^--.*$`)
	rxpCommentBlock               = regexp.MustCompile(`(?ms)^/\*.*\*/`)
	rxpCommentBlockOpen           = regexp.MustCompile(`(?ms)^/\*`)
	rxpNumberFloat1               = regexp.MustCompile(fmt.Sprintf(`^%[1]s\.%[1]s*(e[+-]?%[1]s)?`, rxpSubDec))
	rxpNumberFloat2               = regexp.MustCompile(fmt.Sprintf(`^%[1]s*\.%[1]s+(e[+-]?%[1]s+)?`, rxpSubDec))
	rxpNumberFloat3               = regexp.MustCompile(fmt.Sprintf(`^%[1]s+e[+-]?%[1]s+`, rxpSubDec))
	rxpNumberIntHex               = regexp.MustCompile(fmt.Sprintf(`^0[xX]_?%[1]s+`, rxpSubHex))
	rxpNumberIntOct               = regexp.MustCompile(fmt.Sprintf(`^0[oO]_?%[1]s+`, rxpSubOct))
	rxpNumberIntBin               = regexp.MustCompile(fmt.Sprintf(`^0[bB]_?%[1]s+`, rxpSubBin))
	rxpNumberIntDec               = regexp.MustCompile(fmt.Sprintf(`^%[1]s`, rxpSubDec))
	rxpString                     = regexp.MustCompile(`^'(''|[^'])*'`)
	rxpStringEscapeC              = regexp.MustCompile(`^[eE]'(''|[^'])*'`)
	rxpStringEscapeU              = regexp.MustCompile(`^[uU]&'(''|[^'])*'`)
	rxpStringBin                  = regexp.MustCompile(`^[bB]'[01]*'`)
	rxpStringHex                  = regexp.MustCompile(`^[xX]'[0-9A-Fa-f]*'`)
	rxpStringDollarPrefix         = regexp.MustCompile(`^\$(\pL(\pL|_|\d)*)?\$`)
	rxpIdent                      = regexp.MustCompile(`^\pL(\pL|_|\d|\$)*`)
	rxpIdentQuoted                = regexp.MustCompile(`^"([^"]|"")+"`)
	rxpIdentQuotedU               = regexp.MustCompile(`^[uU]&"([^"]|"")+"`)
	rxpSymbolsSpecial             = regexp.MustCompile(`^[()\[\],;:.]`)
	rxpSymbolsOperatorAll         = regexp.MustCompile("^[+*/<>=~!@#%^&|`?-]")
	rxpSymbolsOperatorNonStandard = regexp.MustCompile("^[~!@#%^&|`?]")
)

var (
	rxpGrpWhitespace = []*regexp.Regexp{
		rxpWhitespace,
	}
	rxpGrpComments = []*regexp.Regexp{
		rxpCommentLine,
		rxpCommentBlock,
	}
	rxpGrpNumbers = []*regexp.Regexp{
		rxpNumberFloat1,
		rxpNumberFloat2,
		rxpNumberFloat3,
		rxpNumberIntHex,
		rxpNumberIntOct,
		rxpNumberIntBin,
		rxpNumberIntDec,
	}
	rxpGrpStrings = []*regexp.Regexp{
		rxpString,
		rxpStringEscapeC,
		rxpStringEscapeU,
		rxpStringBin,
		rxpStringHex,
	}
	rxpGrpIdents = []*regexp.Regexp{
		rxpIdent,
		rxpIdentQuoted,
		rxpIdentQuotedU,
	}
	rxpGrpSymbols = []*regexp.Regexp{
		rxpSymbolsSpecial,
		rxpSymbolsOperatorAll,
	}
)
