# jit3: design

Just In Time Temp Tables

## TODO

- [x] Implement TextSpanner in pg
- [ ] Implement Tokenizer in pg
  - [ ] Create Test Constructors for TokenSpan
- [ ] FIXME pg.spanComments needs to parse block comments as balanced as
      mentioned here.
- [ ] Implement Parser in pg
- [ ] Implement Symbolizer in pg
  - [ ] Implement DataType in pg
  - [ ] Implement Scope in pg & sql
- [ ] Implement CQL
- [ ] Implement pg to CQL translation
- [ ] Design & Implement CQL Optimizer
  - [ ] Look for SQL with SQL with large IO and low size estimate to persist
  - [ ] Look for SQL with low confidence on filtering
- [ ] Implement CQL to pg translation
- [ ] Implement SyntaxEmitter in pg
- [ ] Implement TokenEmitter in pg
- [ ] Implement TokenSpanner in pg
- [ ] Implement Formatter in pg

## Challenges

1. How do we determine which sub-queries should be pre-computed?
2. How do we leverage precomputed tables in queries
   Options Considered
   How to determine sub-queries?
   Q: What is our complete list of candidates?
   A: All permutations of joins, filters, aggregation, and selects

_Q:_ How do we score each candidate?

_A:_ Estimate the size (in bytes) of each candidate looking at joins, filters,
aggression, and selects. This is the storage cost. The IO cost will be the
bytes multiple by the write speed of the system (bytes per second). The CPU
cost is TODO. The benefit is anticipated IO/storage. TODO consider estimation
certainty in algorithm

_Q:_ When to determine sub-queries?

_A:_ Precompute statically based on prior knowledge.

This is what OTBs are. They are really fast when they can be used, but they
often can't be used to serve specific queries.
Generate regularly based on prior knowledge.
This would basically be allowing the system to build OTBs based on usage
statistics in the background. This allows the system to respond to workload
trends automatically but keeps OTB maintenance separate from usage.
Generate dynamically based on current knowledge.
This option allows the system to build OTBs on the fly during use. This gives
the system change to improve cold queries before they run.

## Terminology

### Working with Text

#### Package text

Byte slices representing files are added to text.Texts. Texts become spans
through the text-spanning process.

#### Package span

String segments that can optionally be positioned within a 2D grid. When
positioned, they are TextSpans. Spans that are derived from source text will be
positioned based on its original location in source. Generated spans become
positioned spans through the process of formatting. Texts become spans through
the text-spanning process. Tokens become generated spans through the
token-spanning process.

#### Package token

Tokens represent individual language elements. They are often composed of
one span, but can be compromised from multiple as well. Spans become tokens
through the tokenization process.

#### Package syntax

Syntax nodes are tokens with hierarchy. They group together language concepts
into an associative tree structure. Tokens become syntax nodes through the
parsing process.

#### Package symbol

Symbol nodes are syntax nodes in the context of a scope. Symbols have
resolved identifiers and type information.

### Working with SQL

#### Package dialect

A dialect is a set of implementations of the interfaces needed to work with
SQL text designed to support a particular SQL implementation
(such as Postgres or MySQL).

#### Package translate

Converting from one dialect to another.

#### Package datatype

Defines data types, their representations, capabilities, and conversion rules.

#### Package scope

Defines name-to-object mappings to lookup objects based on their defined names
in a scope.

#### Package object

Defines something that can be named in a scope

## CQL

Is a YAML-based pseudo dialect that represents SQL concepts in their canonical
form. It exists so that code for analyzing and manipulating SQL can be written
once for CQL and various dialects only need to support translating to and from
CQL.

### Expressions

Expressions represent individual values. All expressions have a data type - all
other aspects of expressions are specific to the type of expression.

#### Values

- Ref - a reference to an object in a scope (unary)
- Literal - a literal value (unary)

##### Arithmetic

- Add - addition (binary)
- Subtract - subtraction (binary)
- UnaryMinus - minus (unary)
- Multiply - multiplication (binary)
- Divide - division (binary)
- Mod - modulus division (binary)

##### Equality

- LessThan - less than (binary)
- Equals - equals (binary)
- IsNull - is null (unary)

##### Logic

- Not - logical not (unary)
- And - logical and (binary)
- Or - logical or (binary)

##### Conditional

- Case/When

### Statements

#### Correlate

A join clause. Given two Queryables, produces a new queryable by correlating
them on some condition

#### Filter

A where clause. Given a Queryable, produces a new queryable with only tuples
satisfying a filtering predicate making it through

#### Aggregate

A group by clause. Given a Queryable, produces a new queryable where some
columns are grouped and others are pre-aggregated

#### Project

A select clause. Given a Queryable, produces a new queryable where columns are evaluated

#### Sort

An order by clause. Given a Queryable, produces a new queryable where the tuples
are sorted by a series or orderable expressions
