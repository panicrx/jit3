// Package text provides utilities for working with source text
package text

import (
	"fmt"
	"io"
	"strings"
)

// Pos represents a byte position that is meaningful within the context of a [Texts].
type Pos int

func (p Pos) Plus(n int) Pos {
	return Pos(int(p) + n)
}

func (p Pos) IsValid() bool {
	return p >= 0
}

// Text is a single text object.
type Text struct {
	path string
	data string
	pos  Pos
	end  Pos
}

// Path of this text.
func (t *Text) Path() string {
	return t.path
}

// Reader returns a [io.ReadSeeker] for this text.
func (t *Text) Reader() io.ReadSeeker {
	return strings.NewReader(t.data)
}

// String returns the string representation of this text
func (t *Text) String() string {
	return t.data
}

// Pos implements [gitlab.com/panicrx/jit3/pkg/span.Span.Pos]
func (t *Text) Pos() Pos {
	return t.pos
}

// End implements [gitlab.com/panicrx/jit3/pkg/span.Span.End]
func (t *Text) End() Pos {
	return t.end
}

// Texts is a set of related texts which are related
type Texts struct {
	texts []*Text
}

func (t *Texts) Len() int {
	return len(t.texts)
}

func (t *Texts) Index(n int) *Text {
	return t.texts[n]
}

// Text returns the text containing pos.
//
// Text panics if no text in t contains pos
func (t *Texts) Text(pos Pos) *Text {
	for _, text := range t.texts {
		if pos < text.pos {
			continue
		}

		if pos > text.end {
			break
		}

		return text
	}
	panic(fmt.Sprintf("text: position not found in texts: %v", pos))
}

// Add data to t with path.
//
// Add panics if data is empty or if path is already used.
func (t *Texts) Add(path string, data string) {
	if len(data) == 0 {
		panic("text: data cannot be empty")
	}

	for _, text := range t.texts {
		if text.path == path {
			panic("text: duplicate path: " + path)
		}
	}

	pos := Pos(0)
	if len(t.texts) > 0 {
		pos = t.texts[len(t.texts)-1].end
	}

	txt := Text{
		path: path,
		data: data,
		pos:  pos,
		end:  pos + Pos(len(data)),
	}

	t.texts = append(t.texts, &txt)
}
