package dialect

import (
	"gitlab.com/panicrx/jit3/pkg/scope"
	"gitlab.com/panicrx/jit3/pkg/span"
	"gitlab.com/panicrx/jit3/pkg/symbol"
	"gitlab.com/panicrx/jit3/pkg/syntax"
	"gitlab.com/panicrx/jit3/pkg/text"
	"gitlab.com/panicrx/jit3/pkg/token"
)

type TextSpanner interface {
	SpanTexts(*text.Texts) (*span.TextSpans, error)
}

type Tokenizer[TokenType comparable] interface {
	Tokenize(*span.TextSpans) ([]token.TokenSpan[TokenType], error)
}

type Parser[NodeType comparable, TokenType comparable] interface {
	Parse([]token.TokenSpan[TokenType]) (syntax.Node[NodeType, TokenType], error)
}

type Symbolizer[ObjectType comparable, DataType symbol.DataType, NodeType comparable, TokenType comparable] interface {
	Symbolize(scope.Scope[ObjectType, DataType], syntax.Node[NodeType, TokenType]) (symbol.Node[ObjectType, DataType, NodeType, TokenType], error)
}

type SyntaxEmitter[ObjectType comparable, DataType symbol.DataType, NodeType comparable, TokenType comparable] interface {
	EmitSyntax(symbol.Node[ObjectType, DataType, NodeType, TokenType]) (syntax.Node[NodeType, TokenType], error)
}

type TokenEmitter[NodeType comparable, TokenType comparable] interface {
	EmitTokens(syntax.Node[NodeType, TokenType]) ([]token.Token[TokenType], error)
}

type TokenSpanner[NodeType comparable, TokenType comparable] interface {
	SpanTokens([]token.Token[TokenType]) (span.Spans, error)
}

type Formatter interface {
	Format(span.Spans) (span.TextSpans, error)
}

type Dialect[ObjectType comparable, DataType symbol.DataType, NodeType comparable, TokenType comparable] interface {
	TextSpanner
	Tokenizer[TokenType]
	Parser[NodeType, TokenType]
	Symbolizer[ObjectType, DataType, NodeType, TokenType]
	SyntaxEmitter[ObjectType, DataType, NodeType, TokenType]
	TokenEmitter[NodeType, TokenType]
	TokenSpanner[NodeType, TokenType]
	Formatter
}
