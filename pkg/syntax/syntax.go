package syntax

import "gitlab.com/panicrx/jit3/pkg/token"

type Node[NodeType comparable, TokenType comparable] interface {
	Type() NodeType
}

type LeafNode[NodeType comparable, TokenType comparable] interface {
	Node[NodeType, TokenType]
	Span() token.Token[TokenType]
}

type BranchNode[NodeType comparable, TokenType comparable] interface {
	Node[NodeType, TokenType]
	Len() int
	Node(int) Node[NodeType, TokenType]
}
