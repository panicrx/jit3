package symbol

import "gitlab.com/panicrx/jit3/pkg/syntax"

type DataType interface {
	String() string
}

type Object[ObjectType comparable, DT DataType] interface {
	Type() ObjectType
	String() string
	DataType() DT
}

type NodeType int

const (
	EmptyKind NodeType = iota
	ValueKind
	ObjectKind
)

type Node[ObjectType comparable, DT DataType, SyntaxNodeType comparable, TokenType comparable] interface {
	Type() NodeType
	Node() syntax.Node[SyntaxNodeType, TokenType]
}

type ObjectNode[ObjectType comparable, DT DataType, SyntaxNodeType comparable, TokenType comparable] interface {
	Node[ObjectType, DT, SyntaxNodeType, TokenType]
	Object() Object[ObjectType, DT]
}

type ValueNode[ObjectType comparable, DT DataType, SyntaxNodeType comparable, TokenType comparable] interface {
	Node[ObjectType, DT, SyntaxNodeType, TokenType]
	Value() any
}
