package sql

import "io"

type Marshaler interface {
	MarshalSQL(io.Writer)
}

type Unmarshaler interface {
	UnmarshalSQL(io.Reader)
}
