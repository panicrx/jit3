package scope

import (
	"errors"
	"fmt"

	"gitlab.com/panicrx/jit3/pkg/symbol"
)

var ErrNotFound = errors.New("scope: symbol not found")

func NotFoundErr(name string) error {
	return fmt.Errorf("%w: %s", ErrNotFound, name)
}

type Scope[ObjectType comparable, DT symbol.DataType] interface {
	Lookup(name string) (symbol.Object[ObjectType, DT], error)
}
