package token

import (
	"fmt"

	"gitlab.com/panicrx/jit3/pkg/span"
)

type Token[Type comparable] interface {
	Type() Type
	Span() span.Span

	SubLen() int
	SubSpan(ndx int) span.Span
}

type TokenSpan[Type comparable] interface {
	Token[Type]
	TextSpan() span.TextSpan

	SubTextLen() int
	SubTextSpan(ndx int) span.TextSpan
}

func NewSpan[TokenType comparable](typ TokenType, s ...span.TextSpan) TokenSpan[TokenType] {
	switch len(s) {
	case 0:
		panic("token: missing span")
	case 1:
		return tknSpan1[TokenType]{typ: typ, textSpan: s[0]}
	case 2:
		return tknSpan2[TokenType]{
			typ:          typ,
			textSpan:     span.CombineText(s...),
			subTextSpans: [2]span.TextSpan{s[0], s[1]},
		}
	case 3:
		return tknSpan3[TokenType]{
			typ:          typ,
			textSpan:     span.CombineText(s...),
			subTextSpans: [3]span.TextSpan{s[0], s[1], s[2]},
		}
	default:
		subTextSpans := make([]span.TextSpan, len(s))
		copy(subTextSpans, s)
		return tknSpanN[TokenType]{
			typ:          typ,
			textSpan:     span.CombineText(s...),
			subTextSpans: subTextSpans,
		}
	}
}

type tknSpan1[TokenType comparable] struct {
	typ      TokenType
	textSpan span.TextSpan
}

func (t tknSpan1[TokenType]) Type() TokenType {
	return t.typ
}

func (t tknSpan1[TokenType]) Span() span.Span {
	return t.textSpan.Span()
}

func (t tknSpan1[TokenType]) TextSpan() span.TextSpan {
	return t.textSpan
}

func (t tknSpan1[TokenType]) SubLen() int {
	return t.SubTextLen()
}

func (t tknSpan1[TokenType]) SubSpan(ndx int) span.Span {
	return t.SubTextSpan(ndx).Span()
}

func (t tknSpan1[TokenType]) SubTextLen() int {
	return 1
}

func (t tknSpan1[TokenType]) SubTextSpan(ndx int) span.TextSpan {
	if ndx != 0 {
		panic("token: index out of range")
	}
	return t.textSpan
}

func (t tknSpan1[TokenType]) String() string {
	return fmt.Sprintf("%v(%v)", t.typ, t.textSpan.String())
}

type tknSpan2[TokenType comparable] struct {
	typ          TokenType
	subTextSpans [2]span.TextSpan
	textSpan     span.TextSpan
}

func (t tknSpan2[TokenType]) Type() TokenType {
	return t.typ
}

func (t tknSpan2[TokenType]) Span() span.Span {
	return t.textSpan.Span()
}

func (t tknSpan2[TokenType]) TextSpan() span.TextSpan {
	return t.textSpan
}

func (t tknSpan2[TokenType]) SubLen() int {
	return t.SubTextLen()
}

func (t tknSpan2[TokenType]) SubSpan(ndx int) span.Span {
	return t.SubTextSpan(ndx).Span()
}

func (t tknSpan2[TokenType]) SubTextLen() int {
	return len(t.subTextSpans)
}

func (t tknSpan2[TokenType]) SubTextSpan(ndx int) span.TextSpan {
	return t.subTextSpans[ndx]
}

type tknSpan3[TokenType comparable] struct {
	typ          TokenType
	subTextSpans [3]span.TextSpan
	textSpan     span.TextSpan
}

func (t tknSpan3[TokenType]) Type() TokenType {
	return t.typ
}

func (t tknSpan3[TokenType]) Span() span.Span {
	return t.textSpan.Span()
}

func (t tknSpan3[TokenType]) TextSpan() span.TextSpan {
	return t.textSpan
}

func (t tknSpan3[TokenType]) SubLen() int {
	return t.SubTextLen()
}

func (t tknSpan3[TokenType]) SubSpan(ndx int) span.Span {
	return t.SubTextSpan(ndx).Span()
}

func (t tknSpan3[TokenType]) SubTextLen() int {
	return len(t.subTextSpans)
}

func (t tknSpan3[TokenType]) SubTextSpan(ndx int) span.TextSpan {
	return t.subTextSpans[ndx]
}

type tknSpanN[TokenType comparable] struct {
	typ          TokenType
	subTextSpans []span.TextSpan
	textSpan     span.TextSpan
}

func (t tknSpanN[TokenType]) Type() TokenType {
	return t.typ
}

func (t tknSpanN[TokenType]) Span() span.Span {
	return t.textSpan.Span()
}

func (t tknSpanN[TokenType]) TextSpan() span.TextSpan {
	return t.textSpan
}

func (t tknSpanN[TokenType]) SubLen() int {
	return t.SubTextLen()
}

func (t tknSpanN[TokenType]) SubSpan(ndx int) span.Span {
	return t.SubTextSpan(ndx).Span()
}

func (t tknSpanN[TokenType]) SubTextLen() int {
	return len(t.subTextSpans)
}

func (t tknSpanN[TokenType]) SubTextSpan(ndx int) span.TextSpan {
	return t.subTextSpans[ndx]
}

type tknSpanWithType[TokenType comparable] struct {
	typ TokenType
	tkn TokenSpan[TokenType]
}

func WithTypeSpan[TokenType comparable](tkn TokenSpan[TokenType], typ TokenType) TokenSpan[TokenType] {
	return tknSpanWithType[TokenType]{typ: typ, tkn: tkn}
}

func (t tknSpanWithType[TokenType]) Type() TokenType {
	return t.typ
}

func (t tknSpanWithType[TokenType]) Span() span.Span {
	return t.tkn.Span()
}

func (t tknSpanWithType[TokenType]) TextSpan() span.TextSpan {
	return t.tkn.TextSpan()
}

func (t tknSpanWithType[TokenType]) SubLen() int {
	return t.SubTextLen()
}

func (t tknSpanWithType[TokenType]) SubSpan(ndx int) span.Span {
	return t.tkn.SubSpan(ndx)
}

func (t tknSpanWithType[TokenType]) SubTextLen() int {
	return t.tkn.SubTextLen()
}

func (t tknSpanWithType[TokenType]) SubTextSpan(ndx int) span.TextSpan {
	return t.tkn.SubTextSpan(ndx)
}

var (
	_ TokenSpan[int] = &tknSpan1[int]{}
	_ TokenSpan[int] = &tknSpan2[int]{}
	_ TokenSpan[int] = &tknSpan3[int]{}
	_ TokenSpan[int] = &tknSpanN[int]{}
)
