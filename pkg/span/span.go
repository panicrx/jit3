package span

import (
	"bytes"
	"fmt"

	"gitlab.com/panicrx/jit3/pkg/text"
)

// Span represents a span of text that is an atomic language element.
//
// It differs from [TextSpan] in that it doesn't have a postion within a text yet.
type Span interface {
	String() string
}

type span struct {
	str string
}

func (s span) String() string {
	return s.str
}

func NewSpan(buf []byte) Span {
	return span{str: string(buf)}
}

func NewSpanString(str string) Span {
	return span{str: str}
}

type combined struct {
	spans []Span
}

func Combine(ss ...Span) Span {
	return combine(ss...)
}

func combine[T fmt.Stringer](ss ...T) Span {
	if len(ss) < 1 {
		panic("span: expected 1 or more Span expected")
	}

	var buf bytes.Buffer
	for _, s := range ss {
		buf.WriteString(s.String())
	}
	return NewSpan(buf.Bytes())
}

type Pos struct {
	pos text.Pos
	row int
	col int
}

func (p Pos) Pos() text.Pos {
	return p.pos
}

func (p Pos) Row() int {
	return p.row
}

func (p Pos) Col() int {
	return p.col
}

func NewPos(pos text.Pos, row, col int) Pos {
	return Pos{pos: pos, row: row, col: col}
}

type Range struct {
	pos Pos
	end Pos
}

// Pos denotes the the first position of this range
func (r Range) Pos() Pos {
	return r.pos
}

// End is the first position after this span's content.
func (r Range) End() Pos {
	return r.end
}

func NewRange(pos, end Pos) Range {
	return Range{pos: pos, end: end}
}

// TextSpan is a text entity that spans from point-to-point in a text document.
//
// It typically comes from one of two sources:
//  1. An existing source file that has been tokenized by a [gitlab.com/panicrx/jit3/pkg/dialect.Tokenizer]
//  2. A modified/generated [Span] that has been positioned by a [gitlab.com/panicrx/jit3/pkg/dialect.Formatter]
type TextSpan struct {
	span Span
	rng  Range
}

func NewTextSpan(span Span, rng Range) TextSpan {
	return TextSpan{span: span, rng: rng}
}

func (ts TextSpan) Span() Span {
	return ts.span
}

func (ts TextSpan) Range() Range {
	return ts.rng
}

func (ts TextSpan) String() string {
	return ts.span.String()
}

func CombineText(ts ...TextSpan) TextSpan {
	if len(ts) < 1 {
		panic("span: expected 1 or more TextSpan")
	}

	pos := ts[0].Range().Pos()
	end := ts[len(ts)-1].Range().End()
	span := combine(ts...)

	return NewTextSpan(span, NewRange(pos, end))
}

type Spans struct {
	spans []Span
}

func (ts *Spans) Len() int {
	return len(ts.spans)
}

func (ts *Spans) Span(ndx int) Span {
	return ts.spans[ndx]
}

type TextSpans struct {
	Texts *text.Texts
	spans []TextSpan
}

func NewTextSpans(texts *text.Texts, spans []TextSpan) *TextSpans {
	return &TextSpans{Texts: texts, spans: spans}
}

func (ts *TextSpans) Len() int {
	return len(ts.spans)
}

func (ts *TextSpans) Span(ndx int) TextSpan {
	return ts.spans[ndx]
}
