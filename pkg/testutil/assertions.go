package testutil

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/panicrx/jit3/pkg/span"
	"gitlab.com/panicrx/jit3/pkg/token"
)

func AssertPosEquals(t *testing.T, expectedI, actualI any, _ ...any) bool {
	t.Helper()

	expected, ok := ConvertAndAssertType[span.Pos](t, expectedI)
	if !ok {
		return false
	}

	actual, ok := ConvertAndAssertType[span.Pos](t, actualI)
	if !ok {
		return false
	}

	if !assert.Equal(t, expected.Col(), actual.Col()) {
		return false
	}

	if !assert.Equal(t, expected.Row(), actual.Row()) {
		return false
	}

	return true
}

func ConvertAndAssertType[expectedType any](t *testing.T, actual any) (expectedType, bool) {
	t.Helper()

	expected := new(expectedType)
	typ := reflect.TypeOf(expected)
	switch typ.Elem().Kind() {
	case reflect.Interface:
		if !assert.Implements(t, expected, actual) {
			return *expected, false
		}
	default:
		if !assert.IsType(t, *expected, actual, "Expected result to be of type %s but it is a %T", typ.Elem().Name(), actual) {
			return *expected, false
		}
	}

	return actual.(expectedType), true
}

func AssertSpanEquals(t *testing.T, expectedI, actualI any, _ ...any) bool {
	t.Helper()

	expected, ok := ConvertAndAssertType[span.Span](t, expectedI)
	if !ok {
		return false
	}

	actual, ok := ConvertAndAssertType[span.Span](t, actualI)
	if !ok {
		return false
	}

	if !assert.Equal(t, expected.String(), actual.String()) {
		return false
	}

	return true
}

func AssertTextSpanEquals(t *testing.T, expectedI, actualI any, _ ...any) bool {
	t.Helper()

	expected, ok := ConvertAndAssertType[span.TextSpan](t, expectedI)
	if !ok {
		return false
	}

	actual, ok := ConvertAndAssertType[span.TextSpan](t, actualI)
	if !ok {
		return false
	}

	if !AssertSpanEquals(t, expected.Span(), actual.Span()) {
		return false
	}

	if !AssertRangeEquals(t, expected.Range(), actual.Range()) {
		return false
	}

	return true
}

func AssertTokenSpanEquals[TokenType comparable](t *testing.T, expectedI, actualI any, _ ...any) bool {
	t.Helper()

	expected, ok := ConvertAndAssertType[token.TokenSpan[TokenType]](t, expectedI)
	if !ok {
		return false
	}

	actual, ok := ConvertAndAssertType[token.TokenSpan[TokenType]](t, actualI)
	if !ok {
		return false
	}

	if !AssertTextSpanEquals(t, expected.TextSpan(), actual.TextSpan()) {
		return false
	}

	return true
}

func AssertTokenSpansEquals[TokenType comparable](t *testing.T, expectedI, actualI any, _ ...any) bool {
	t.Helper()

	expected, ok := ConvertAndAssertType[[]token.TokenSpan[TokenType]](t, expectedI)
	if !ok {
		return false
	}

	actual, ok := ConvertAndAssertType[[]token.TokenSpan[TokenType]](t, actualI)
	if !ok {
		return false
	}

	if !assert.Len(t, actual, len(expected)) {
		return false
	}

	for n := range expected {
		if !AssertTextSpanEquals(t, expected[n], actual[n]) {
			return false
		}
	}

	return true
}

func AssertRangeEquals(t *testing.T, expected span.Range, actual span.Range, _ ...any) bool {
	t.Helper()

	switch {
	case !AssertPosEquals(t, expected.Pos(), actual.Pos(), "range start matches"):
		return false
	case !AssertPosEquals(t, expected.End(), actual.End(), "range end matches"):
		return false
	default:
		return true
	}
}
